<?php 
    require("config.php"); 
    $submitted_username = ''; 
    if(!empty($_POST)){ 
        $query = " 
            SELECT 
                id, 
                username, 
                password, 
                salt, 
                email 
            FROM users 
            WHERE 
                username = :username 
        "; 
        $query_params = array( 
            ':username' => $_POST['username'] 
        ); 
         
        try{ 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); } 
        $login_ok = false; 
        $row = $stmt->fetch(); 
        if($row){ 
            $check_password = hash('sha256', $_POST['password'] . $row['salt']); 
            for($round = 0; $round < 65536; $round++){
                $check_password = hash('sha256', $check_password . $row['salt']);
            } 
            if($check_password === $row['password']){
                $login_ok = true;
            } 
        } 

        if($login_ok){ 
            unset($row['salt']); 
            unset($row['password']); 
            $_SESSION['user'] = $row;  
            header("Location: index.html"); 
            die("Redirecting to: index.html"); 
        } 
        else{ 
            print("Login Failed."); 
            $submitted_username = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8'); 
        } 
    } 
?> 

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="img/logo.png">
    <title>Project Pals | Dashboard</title>

    <meta name="description" content="Bootstrap Tab + Fixed Sidebar Tutorial with HTML5 / CSS3 / JavaScript">
    <meta name="author" content="Untame.net">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
     <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen   animated fadeInDown">
        
            <div>

                 <img src="img/logo.png" class="img-rounded" alt="Project Pals" >


            </div>
           <h3>Welcome to Project Pals</h3>
            <p>The primary objective of Project Pals is to inspire and train students to become lifelong learners with the capabilities to methodically approach problem solving situations 
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in. To see it in action.</p>


    
    <form action="index.php" method="post" role="form" class="m-t"> 

        <div class="form-group">
        
        <input type="text" placeholder="Username" name="username" value="<?php echo $submitted_username; ?>" class="form-control" required=""/> 
        </div>
        
        
        <div class="form-group">
      
        <input type="password" placeholder="Password" name="password" class="form-control"  value="" required=""/> <br /><br />
        </div>



        
        
        <button type="submit" class="btn btn-primary block full-width m-b">Login</button> 
         <a class="btn btn-sm btn-white btn-block" href="register.php">Create an account</a>
   <p class="m-t"> <small>Project Pals &copy; 2015</small> </p>
    </form>


</div>

</body>
</html>