

var about = function(){
	var self = this;

};

var chatmessage = function(msg){
	var self = this;
	console.log(msg);

};

var createHandler = function(x){

};

exports.Chat = function(socket, app){
	this.app = app;
	this.socket = socket;

	this.handlers = {
		about: about.bind(this),
		'chat message': chatmessage.bind(this),
		'app/dashboard/profile/create': createHandler.bind(this)


	};

};

//singleton, exports.Profile = new Profile();
/*
function Profile(socket, app){
	

}
*/
