module.exports = function(){
  
  var com = require('../app');
  var DB = require("../database/DB");
  var cookieParser = require('cookie-parser');
  var User = require("../User/User");


  var ChatMessage = function(userName, message){
    this.userName = userName;
    this.message = message;
    this.timestamp = Date.now();
  }

  var Project = function(id){
    id = id + "";
    return {
      id: id,
      chats: [],
      //connected users
      users: []
    }
  }

  var SocketProject = function(){
    this.users = {};
  }

  //using socket.id to map users 
  //SECURITY FLAW
  var SocketUser = function(name){
    this.name = name;
  }

  var projects = [];

  for (var i = 0; i < 4; i++) {
    projects.push(new Project(i + 1));
  }

  console.log(projects);

DB().get({bucket:"projects",key:"id"},projects, function(err,res){
  if (err) throw new Error(err);
  
  //assume
  if (res[0].isNotFound){
    DB().push({bucket:"projects", key: "id"}, projects, function(err,rslt){
      if (err) throw new Error(err);
      console.log(rslt);
    });
  }

//  console.log(res[0].values.shift().value.chats);
  
});

 



/*
DB().push({bucket:"projects",key:"id"},projects, function(err, res){
  if (err) throw new Error(err);
  console.log(res);
});
*/
/*
DB().debugBucket({bucket:"projects", stream:false}, function(err,res){
  console.log(err);
  console.log(res);
});
*/
/* 
 DB().debugBuckets({},function(err,res){
  console.log(res);
 });
*/
/*
DB().debugRemoveAll({bucket:"projects"}, function(err,rs){
  if (err) throw new Error(err);

});
*/
  var ioAuth = function(socket,next){
    //console.log(socket.request.session);
    var user = new User(socket.request);

    if (!user.Session.isAuth()){
      return console.log("authentication failure");
    }
    else {
      return next();  
    }
    
  }
  //define namespaces
  var namespaces = {
    dashboard: com.io.of('/dashboard'),
    workspace: com.io.of('/workspace')
  };

  Object.keys(namespaces).forEach(function(key){
    namespaces[key].use(ioAuth);
  });
  //var dashboard = com.io.of('/dashboard');
  //var workspace = com.io.of('/workspace');

//new io Authentication? make an object?
  //dashboard.use(ioAuthentication);
  //workspace.use(ioAuthentication);

    // usernames which are currently connected to the chat
  //var usernames = {};
  var SocketProjects = {};

  // rooms which are currently available in chat
//  var rooms = ['room1','room2','room3'];

  namespaces.workspace.on('connection', function(socket){
     
    
    // when the client emits 'adduser', this listens and executes
    socket.on('adduser', function(projectID){
      
      // store the username in the socket session for this client
      socket.username = socket.request.session.user;
      // store the room name in the socket session for this client
      socket.room = projectID;
      
      DB().get({bucket:"projects"},socket.room, function(err,res){
        if (err) throw new Error(err);
        res = res.shift().values.shift();
        res.value.users.push(socket.username);
        DB().update(res, function(err,rslt){
          if (err) throw new Error(err);
          console.log(rslt);
        }); 

        
      });

    //create new project if not defined
      if (typeof SocketProjects[projectID] === 'undefined'){
        SocketProjects[projectID] = new SocketProject();
      }

      // add the client's username to the global list
      //usernames[socket.id] = socket.username;
      SocketProjects[projectID].users[socket.id] = new SocketUser(socket.username);
      //usernames[socket.id] = new SocketUser(socket.username);
      // send client to room 1
      socket.join(projectID);


      // echo to client they've connected
      socket.emit('updatechat', 'SERVER', 'you have connected to ' + projectID);

      var usernames = SocketProjects[socket.room]["users"] || {};

      com.io.of('/workspace').in(projectID).emit('updateusers', usernames);
      //com.io.in.(projectID).emit('updateusers', "debug");
      socket.broadcast.to(projectID).emit('updatechat', 'SERVER', socket.username + ' has connected to this room');
      //socket.emit('updaterooms', rooms, 'room1');
    });

  socket.on('error', function(err){
    console.log("error", err);
  });
    
    // when the client emits 'sendchat', this listens and executes
    socket.on('sendchat', function (data) {
      console.log(data);

      DB().get({bucket:"projects"},socket.room, function(err,res){
        if (err) throw new Error(err);
        res = res.shift().values.shift();
        console.log(res);
        var chats = res.value.chats;
        chats.push(new ChatMessage(socket.username, data));
        
        console.log(res);
        DB().update(res, function(err,rslt){
          if (err) throw new Error(err);
          console.log(rslt);
        }); 

        
      });

      // we tell the client to execute 'updatechat' with 2 parameters
      socket.broadcast.to(socket.room).emit('updatechat', socket.username, data);
    });
    /*
    socket.on('switchRoom', function(newroom){
      socket.leave(socket.room);
      socket.join(newroom);
      socket.emit('updatechat', 'SERVER', 'you have connected to '+ newroom);
      // sent message to OLD room
      socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username+' has left this room');
      // update socket session room title
      socket.room = newroom;
      socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username+' has joined this room');
      socket.emit('updaterooms', rooms, newroom);
    });
    
*/
    // when the user disconnects.. perform this
    socket.on('disconnect', function(){

      DB().get({bucket:"projects"},socket.room, function(err,res){
        if (err) throw new Error(err);
        res = res.shift().values.shift();
        //res.value.users.push(socket.username);
        var users = res.value.users;
        users.splice(users.indexOf(socket.username),1);
        DB().update(res, function(err,rslt){
          if (err) throw new Error(err);
          console.log(rslt);
        }); 

        
      });
      // remove the username from global usernames list
      delete SocketProjects[socket.room].users[socket.id];

      //if project has no users
      if (Object.keys(SocketProjects[socket.room]["users"]).length === 0){
        //remove the project
        delete SocketProjects[socket.room];
      }

      var usernames = null;

      if (typeof SocketProjects[socket.room] === "undefined"){
        usernames = {};
      }
      else {
        usernames = SocketProjects[socket.room]['users'];
      }
      
      //delete usernames[socket.id];
      // update list of users in chat, client-side
      
      //com.io.in.(socket.room).emit('updateusers', usernames);
      com.io.of('/workspace').in(socket.room).emit('updateusers', usernames);
      // echo globally that this client has left
      
      socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username + ' has disconnected');
      socket.leave(socket.room);
    });

  });

/*
//uses passportjs info in session store to provide authentication to socket.io
function ioAuthentication(socket, next) {
  
    console.log('Socket.io authentication running ' + socket.nsp.name);
  // create the fake req that cookieParser will expect 
            
  var req = {
    "headers": {
      "cookie": socket.request.headers.cookie,
    },
  };


  com.cookieParser('keyboard cat')(req, null, function(err) {
    //if exception is thrown
    return next('error: ' + err, false);
  });
  
  com.sessionStore.get(req.signedCookies['connect.sid'] || req.cookies['connect.sid'], function(err, cookie){
    
    if (typeof cookie === 'undefined' || Object.keys(cookie.passport).length === 0){
      //probably log more info about attempted auth
      console.log('Bad socket authentication');
      //stop connectoin, bad authentication
      return next('bad auth', false);
    }
    

    socket.sessionID = req.signedCookies['connect.sid'] || req.cookies['connect.sid'];
    socket.sessionUser = cookie.passport.user;

    console.log(socket.sessionUser + " authenticated.");
    
    
    return next();
  });

}
*/
  
function bindHandlers(socket, eventHandlers){
  for (var category in eventHandlers){
      var handler = eventHandlers[category].handlers;
      
      for (var event in handler){
        socket.on(event, handler[event]);
      }
    }

}


/*
  dashboard.on('connection', function(socket){
    //require dashboard handlers
    var profile = require('./dashboard/profile');

    var eventHandlers = {
      'profile': new profile.Profile(socket, com.app)

    };

    bindHandlers(socket,eventHandlers);
    

    console.log(socket.sessionUser + " connected.");

    socket.on('disconnect', function(socket){
      console.log(socket.sessionUser + " disconnected.");
    });
  });

  workspace.on('connection', function(socket){
    //require workspace handlers
    var chat = require("./workspace/chat");

    var eventHandlers = {
      //no handlers written
      "chat": new chat.Chat(socket, com.app)

    };
    
    bindHandlers(socket,eventHandlers);

    console.log(socket.sessionUser + " connected.");

    socket.on('disconnect', function(socket){
      console.log(socket.sessionUser + " disconnected.");
    });
  });
*/


}();