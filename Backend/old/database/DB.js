var pool = require('./dbCon');
var Q = require('q');

/*
API USE
var MysqlDB = require('./database/MysqlDB');
MysqlDB.getAllUsers().then(function(users){
  console.log(users);
});
*/




function MysqlDB (options){}
MysqlDB.prototype.asyncQuery = function(sql) {
        return function(){
            var result = Q.defer(),
                paramsArray = [].slice.call(arguments);
            pool.getConnection(function(err, con) {
                if (err) result.reject(err);
               // con.query(sql, paramsArray, result.makeNodeResolver());
                con.query(sql, paramsArray, function(err, rows){
                    if (err) throw err;
                    if (rows){
                        
                      result.resolve(rows);
                    }
                    con.release();
                    
              });
            });
            return result.promise;
        }
}
/*
MysqlDB.prototype = {
    getAllUsers: MysqlDB.prototype.asyncQuery('SELECT * FROM Users'),
    getFirstName: MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE FirstName = ?'),
    getUserPassword: MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE password = ?'),
    getUserName: MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE UserName = ?'),
    getUserEmail: MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE Email = ?')
}
*/


MysqlDB.prototype.getAllUsers = MysqlDB.prototype.asyncQuery('SELECT * FROM Users');
MysqlDB.prototype.getFirstName = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE FirstName = ?');
MysqlDB.prototype.getUserPassword = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE UserName = ? AND password = ?');
MysqlDB.prototype.getUserName = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE UserName = ?');
MysqlDB.prototype.getUserEmail = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE Email = ?');
MysqlDB.prototype.insertNewUser = MysqlDB.prototype.asyncQuery("INSERT INTO  `c9`.`Users` (" +
                                                    "`id` ,`Email` ,`LastName` ,`FirstName` ,`UserName` ,`classID` ,`password` ,"+
                                                    "`lastLogin` ,`GlobalPermission` ,`grade` ,`birthDate`)"+
                                                    "VALUES ("+
                                                    "NULL, ?,?,?,?,?,?, '',?,'','');");
                                                    //Email, LastName, FirstName, UserName, classID, password, GlobalPermission

function createApplication(options){
    //choose between different connections here, mysql, mssql
    if (!options){
        throw "Must specify db connection string as constructor argument.  mysql (my), mongo (mn), mssql (ms)";
    }
    if (options === "mysql" || options === "my"){
        return new MysqlDB(options);    
    }
    else{
        throw "Incorrect options usage, see DB docs";
    }
    
}
//expose factory
module.exports = createApplication;