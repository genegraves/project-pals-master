//var com = require('./mainRebuild');
var config = require('./config');
var mysql = require('mysql');
var Q = require('q');

var pool = mysql.createPool({
  database: config.mysqlConfig.database,
  host : config.mysqlConfig.host,
  user : config.mysqlConfig.user,
  password : config.mysqlConfig.password
  
});
/*
function Base(){
    this.results = [];
	this.query = 'query here';
	this.asyncQuery = function(query){
	        var defer = com.Q.defer();
	        com.pool.getConnection(function(err, con) {

            if (err) defer.thenReject(err);
              con.query(query, defer.makeNodeResolver());
              
            }.bind(this));
            return defer.promise;
	}
}
*/

function DB (options){
    this.asyncQuery = function(sql) {
        return function(){
            var result = Q.defer(),
                paramsArray = [].slice.call(arguments);
            pool.getConnection(function(err, con) {
                if (err) result.reject(err);
               // con.query(sql, paramsArray, result.makeNodeResolver());
                con.query(sql, paramsArray, function(err, rows){
                    if (err) throw err;
                    if (rows){
                        
                      result.resolve(rows);
                    }
                    con.release();
                    
              });
            });
            return result.promise;
        }
    };

   this.getAllUsers = this.asyncQuery('SELECT * FROM Users');
   this.getFirstName = this.asyncQuery('SELECT * FROM Users WHERE FirstName = ?');

	
}


//inherit base
//DB.prototype = new Base();

//factory for DB (allows chaining)
function createApplication(options){
    return new DB(options);
}

//expose createApplication

module.exports = createApplication;






/*
this.go = function(query){
        com.pool.getConnection(function(err, con) {
    
            if (err) throw err;
             con.query(query, function(err, rows){
                if (err) throw err;
                if (rows){
                    
                  for (var i = 0; i < rows.length; i++){
                    this.results.push(rows[i]);
                    
                  }
                }
                con.release();
                return this.results;
              });
            });
    }
    */