var common = {
  express: require('express'),
  session: require('express-session'),
  SessionStore: require('express-mysql-session'),
  bodyParser: require('body-parser'),
  cookieParser: require('cookie-parser'),
  path: require('path'),
  config: require('./config'),
  mysql: require('mysql'),
  passport: require('passport'),
  LocalStrategy: require('passport-local').Strategy,
  flash: require('connect-flash'),
  bcrypt: require('bcrypt-nodejs'),
  fs: require('fs'),
  http: require('http'),
  https: require('https'),
  logger: require('./Logger')
  
};


common.pool = common.mysql.createPool({
  database: common.config.mysqlConfig.database,
  host : common.config.mysqlConfig.host,
  user : common.config.mysqlConfig.user,
  password : common.config.mysqlConfig.password
  
});

var express = require('express');
var session = require('express-session');
var SessionStore = require('express-mysql-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var path = require('path');
var config = require('./config');
var mysql = require('mysql');

var pool = mysql.createPool({
  database: config.mysqlConfig.database,
  host : config.mysqlConfig.host,
  user : config.mysqlConfig.user,
  password : config.mysqlConfig.password
  
});

module.exports = pool;



/*
var con = mysql.createConnection(config.mysqlConfig);
con.connect();

con.query('SELECT * FROM Users', function(err, rows, fields){
  if (err) throw err;
  
  if (rows) {
    for (var i = 0; i < rows.length; i++){
      console.log(rows[i]);
    }
  }
});

con.end();

*/
//var pool = mysql.createPool(config.mysqlConfig);


var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
var bcrypt = require('bcrypt-nodejs');

var fs = require('fs');
var http = require('http');
var https = require('https');

//singleton used for permanent logging purposes / logging to file etc
var logger = require('./Logger.js');

var root = path.normalize(__dirname + '/../Frontend');

//sessionUser password: Mx5JYdRKaWPSyhbS

//ALL HTTPS OPTIONS DISABLED
/*
var sslOptions = {
  key: fs.readFileSync('backend/ssl/server.key', 'utf8'),
  cert: fs.readFileSync('backend/ssl/server.crt', 'utf8'),
  "passphrase": "projectpals"
};
*/

var app = express();
 
var port = process.env.PORT || 3000;
var httpsPort = process.env.SSLPORT || 8443;

//var httpsServer = https.createServer(sslOptions, app);
//httpsServer.listen(httpsPort);
var server =  http.createServer(app).listen(config.port);
logger.log('HTTP server started ' + config.port);
//logger.log('HTTPS server started ' + httpsPort);

// Set up the Socket.IO server, HTTP
var io = require('socket.io').listen(server);

app.use(express.static(root));
//for https IO server, not working
/*
var io = require('socket.io').listen(httpsServer, {
  "log level": 3,
  "match origin protocol" : true,
  "transports" : ['websocket']
});
*/


var u = require('./User.js');
//var u = new user();
//var sessionStore = new session.MemoryStore();
var sessionStore = new SessionStore(config.sqlSessionOptions);
 
 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cookieParser());

var sessionMiddleware = session({
	store: sessionStore,
	secret: 'keyboard cat'
});

app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


/*
//HTTPS REDIRECT
app.use(function (req,res,next){
  if (!req.secure){
    var requestHost = req.headers.host.split(':');
    requestHost = requestHost[0];
    var redirectURL = 'https://' + requestHost + ':' + httpsPort + req.url;
    console.log(redirectURL);
    return res.redirect(redirectURL);
  }
  next();
});

*/
app.get('/', function(req,res,next){
  console.log(root);
	res.redirect(root + "/login");
	
});

//requiring routes this way is redundant
//will FIX with express.router() and by using circular dependencies before this friday

require('./socketRoutes/socketCon')(app, io, cookieParser, sessionStore, logger, pool);

require('./routes/passportConfig')(app, LocalStrategy, passport, u, logger, pool);

require('./routes/dashboard')(app, passport, logger, root, pool);

require('./routes/workspace')(app, passport, logger, root, pool);

require('./routes/login')(app, passport, logger, root, pool);

require('./routes/logout')(app, passport, logger, root, pool);












