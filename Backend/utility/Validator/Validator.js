//need to finish up adding errors, 

//useage
/*
*var x = new Validator();
*
*
*/

var Validator = function (options){
	var _options = {
		form: options.form ? options.form : null,
		genAllErrorLabels: options.genAllErrorLabels ? options.genAllErrorLabels : false,
		defaultMessages: {
			phone: "Please enter a phone number.",
			minLength: "Field length must be at least ",
			maxLength: "Field length cannot be greater than ",
			digits: "Field must only be digits.",
			empty: "Field must not be empty.",
			email: "Field must be formatted properly."
		}
	};	


	var formError = false;
	var currErrors = [];

	var registeredElems = {};
	var currElement = null;
	
	var Validate = function(){

	}

	Validate.prototype = {
		//run a single rule
		runElemRules: function(name){
			//remove old errors
			registeredElems[name].errors = [];
			//for each rule			
			for (var i = 0; i < registeredElems[name].rules.length; i++) {
				//if rule is false, invalid
				var rule = registeredElems[name].rules[i];

				var ran = rule(registeredElems[name]);

				//failure
				if (typeof ran === 'object'){
					//formError = true;
					registeredElems[name].errors.push(ran);
					
				}
				//success
				else {
					
					
				}				
				

			}

			if (registeredElems[name].errors.length == 0){
				//no errors 
				registeredElems[name].callback(false, null);
			}
			else{
				//yes errors
				formError = true;
				registeredElems[name].callback(true, registeredElems[name].errors);
				
				
			
			}
			if (_options.genAllErrorLabels){

					this.addErrors(registeredElems[name].ref, registeredElems[name].errors);
				}


		},
		//fire on form submit, run all rules
		runAll: function(){

			for (var key in registeredElems){
				this.runElemRules(key);
			}
		},
		addErrors: function(elem, errors){
			var next = util.next(elem);
			if (errors.length == 0){
				next.style.display = 'none';
				next.innerHTML = '';
			}
			else{
				
				next.style.display = 'block';
				next.innerHTML = "<ol>";
				for (var i = 0; i < errors.length; i++) {

					next.innerHTML += "<li>" + errors[i].message + "</li>";
				}
				next.innerHTML += "</ol>";
			}
			next = null;
			
		}
	};

	
	
	var Util = function(){

	}

	Util.prototype = {
		listen: function(el,event,callback){

			el.addEventListener(event,callback,true);
		},
		//equivalent to jquery next
		next: function(elem){	
			
			
			var nodes = Array.prototype.slice.call(elem.parentNode.children);
			var index = nodes.indexOf(elem);
			//out of range
			
			if (index + 1 == nodes.length){
				return false;
			}
			else{
				var nextElem = nodes[index + 1];

				nodes = null;
				index = null;
				return nextElem;
			}
		},
		simpleMixin: function(target,source){
			
			for (var key in source){
				if (target.hasOwnProperty(key)){
					target[key] = source[key];
				}
			}


		}

	};

	var util = new Util();
	var validate = new Validate();

	var rules = function(Validator) {
	  this.Validator = Validator;
	  return this;
	}

	rules.prototype = {
	  /**
	   * Description
	   * @method phone
	   * @param {} field
	   * @return MemberExpression
	   */
	  phone: function(message) {

	  	registeredElems[currElement.name].rules.push(function(reg){
	  		
	  		if (!reg.ref.value.match(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/)){
	  			return {type: 'phone', message: message || _options.defaultMessages.phone};
	  		}
	  		return false;
	  	});
	  	/*
	      ///^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/
	    var fieldName = this.Validator.util.getLastField();
	    if (!fieldName){
	        //undef already handled
	        return this.Validator;
	    }
	    for (var first in fieldName.val) break;
	    
	    //matches
	    //8888888888
	    //888-888-8888
	    //(888)8888888
	    //(888)888-8888
	    if (!fieldName.val[first].match(/^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/)){
	        formError = true;
	        fieldName.err.push("phone: " + fieldName.val[first]);
	    }
	    */
	    
	    return this.Validator;
	  },
	  /**
	   * 
	   * @method minLength
	   * @param {int} min
	   * @return MemberExpression
	   */
	  minLength: function(min, message){
	    registeredElems[currElement.name].rules.push(function(reg){
	  		
	  		if (reg.ref.value.length < min){
	  			return {type: 'minLength', message: message || _options.defaultMessages.minLength + min + '.'};
	  		}
	  		return false;
	  	});
	    return this.Validator;
	  },
	  /**
	   * Description
	   * @method maxLength
	   * @param {} max
	   * @return MemberExpression
	   */
	  maxLength: function(max, message){
	  	 registeredElems[currElement.name].rules.push(function(reg){
	  		
	  		if (reg.ref.value.length > max){
	  			return {type: 'maxLength', message: message || _options.defaultMessages.maxLength + max + '.'};
	  		}
	  		return false;
	  	});

	    return this.Validator;
	  },
	  /**
	   * Checks wether string is all digits
	   * @method digits
	   * @return MemberExpression
	   */
	  digits: function(message){
	  	registeredElems[currElement.name].rules.push(function(reg){
	  		
	  		if (!reg.ref.value.match(/^\d+$/)){
	  			return {type: 'digits', message: message || _options.defaultMessages.digits};
	  		}
	  		return false;
	  	});

	    return this.Validator;
	  },
	  
	  /**
	   * Description
	   * @method empty
	   * @return MemberExpression
	   */
	  empty: function(message){

	  	registeredElems[currElement.name].rules.push(function(reg){
	  		
	  		if (reg.ref.value.length === 0){
	  			return {type: 'empty', message: message || _options.defaultMessages.empty};
	  		}
	  		return false;
	  	});

	    return this.Validator;
	  },
	  /**
	   * Description
	   * @method email
	   * @return MemberExpression
	   */
	  email: function(message){
	      ///^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	    

	    registeredElems[currElement.name].rules.push(function(reg){
	  		
	  		if (!reg.ref.value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
	  			return {type: 'email', message: message || _options.defaultMessages.email};
	  		}
	  		return false;
	  	});

	    return this.Validator;

	  },
	  customRule: function(ruleFunc){
	  	registeredElems[currElement.name].rules.push(ruleFunc);
	  	return this.Validator;
	  }

	};

	if (options && typeof options.defaultMessages === 'object'){
		
		util.simpleMixin(_options.defaultMessages, options.defaultMessages);

	}


	//exposed API
	return new function validator(form){
		
		

		this.rules = new rules(this);

		validator.prototype.add = function(elem,event,callback){
			
			if(typeof elem === 'object'){

				//make sure to remove reference later!
				registeredElems[elem.name] = {
					ref: elem,
					rules: [],
					callback: callback,
					errors: []
				};

				currElement = elem;


				if (_options.genAllErrorLabels){
					this.generateLabel();
				}


				(function (self){					
					util.listen(elem,event,function(e){
						validate.runElemRules(elem.name);
					});
				})(this);
			}

			return this;
		}

		/*
			unfinished, instead of genAllErrorLabels, can call this function 
			which has life callbacks
		*/
		validator.prototype.generateLabel = function(onCreate, onAttached){
			//console.log('working');
			var label = document.createElement("LABEL");

			if (onCreate && typeof onCreate === 'function'){
				onCreate(label);
			}

			label.className = label.className || 'error';
			label.innerText = '';
			label.htmlFor = currElement.name;
			label.style.display = 'none';
			//insert next if there is a next
			if (currElement.nextSibling){
				currElement.parentNode.insertBefore(label, currElement.nextSibling);
			}
			else{
				currElement.parentNode.appendChild(label);
			}

			if (onAttached && typeof onAttached === 'function'){
				onAttached(label);
			}
			
			label = null;
			
			return this;
		}

		validator.prototype.submit = function(callback){
			//handle other validation
			util.listen(_options.form,'submit',function(e){
				e.preventDefault();
				//set form to false and reevaluate
				//a bad rule will set to true
				formError = false;
				validate.runAll();
				if (!formError){
					callback(_options.form);
				}
				//callback();
			});
		}




	}




}

// .addField(elem, event, errorCallback)
//		.addRule("ruleType" || customFunction)
