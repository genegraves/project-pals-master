
var db = require('./database/DB');
module.exports = function(){
  
  var com = require('./app');
  
    com.passport.use(new com.LocalStrategy(
  function(username, password, done) {
  	
    //either successfully finds a user or generates an error response to request
    //fix so its all chainable with promises?
    db.MysqlDB().getUserName(username, function(err, users){
      //handle error
      if (users.length == 0){
        //user not found
        return done(null, false, { message: 'Incorrect username.' });
      }
      else{
        
        db.MysqlDB().getUserPassword(users[0].UserName, password, function(err, user){
          if (user.length == 0){
            return done(null, false, { message: 'Incorrect password.' });
          }
          else{
            return done(null, user[0]);
          }
        });
        
      }
    });
    
  /*
  	com.u.findOne(username, function(err, user){
  		if (err){ 
  			
  			return done(err); 

  		}
  		
  		if (!user){
  			
  			return done(null, false, { message: 'Incorrect username.' });
  		}
  		
  		if (!com.u.validPassword(password)){

  			
  			return done(null, false, { message: 'Incorrect password.' });
  		}
  		
  		return done(null, user);
  	});
    */
  }
));

//adds user to passports session store
//adds user.username to req.user?
com.passport.serializeUser(function(user, done) {
	
	
  done(null, user.UserName);
});



com.passport.deserializeUser(function(id, done) {
  
  
  done(null, id);
  //eventually should check SESSION STORE to see if user exists on request
  /*
  u.findById(id, function(err, user){
  	done(err, user);
  });
*/

});
    
}();