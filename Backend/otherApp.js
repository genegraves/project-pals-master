//to run server navigate to directory of app.js and in terminal run "node app.js" which executes the file with node


//require is part of node.js, it functions as a header directive
//like include, using, import
var express = require('express');
var app = express();
var path = require('path');



var root = path.normalize(__dirname + '/../Frontend');

//this is a middleware route
//routing allows for dynamic resolution of requests
//if a request asks for '/' the following middleware currently prints 'in root route'
//so effectively it isn't doing anything yet
//routing works top down
//each route is executed consecutively
//overriding works this way
//check out http://expressjs.com/starter/basic-routing.html


app.get('/',function(req, res, next){
   console.log('in root blah blah blah');
   
   //next means middleware finished, go to next (which is express.static), so all other requests hosted statically
   next(); 
});

app.get('/',function(req, res, next){
   console.log('in root');
   
   //next means middleware finished, go to next (which is express.static), so all other requests hosted statically
   next();
});

app.get('/project/:id', function(req,res,next){
   console.log(req.params.id);
	res.redirect('/worker/' + req.params.id);
	
});

app.get('/worker/*', function(req,res,next){
   res.send('all projects');
   next();
});


//this starts a static web server
app.use(express.static(root));

//below tells app to start server and listen at environment port OR port 3000
//process.env.PORT will always be used in c9.io as its a managed environment
//therefore access via projectpals-mebogler.c9.io
//normally would be localhost:port
app.listen(process.env.PORT || 3000);