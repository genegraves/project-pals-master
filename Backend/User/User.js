var rndm = require('rndm');
var crypto = require('crypto');
var uid = require('uid-safe');
var escape = require('base64-url').escape;
var DB = require("../database/DB");
var bcrypt = require('bcrypt');
console.log(bcrypt);

var UserOptions = {
	secretRounds: 10,
	saltRounds: 10,
	saltSize: 20,
	pre: "$2a$10$"
};




var User = function(req, res){
	//synchronous
	//new secret per session
	//secret = req.session.secret = req.session.secret 
		//|| crypto.randomBytes(UserOptions.secretLength).toString('base64');
		//uid.sync(UserOptions.secretLength);

	
		//rndm(UserOptions.saltLength);
	this.Session = new Session(req,res);
	

	



	User.prototype.register = function(callback){
		
		

		
		//validate username here
		//remove XSS

		
		DB().getUserName(req.body.Username, function(err, rows){
			if (err) return callback(err);
			if (rows.length != 0){
				return callback(false, false,"Username exists.");

			}
			//assert user does exist
			else {
				DB().getUserEmail(req.body.Email, function(err,res){
					if (err) return callback(err);
					if (res.length != 0){
						return callback(false,false,"Email exists.");
					}

					bcrypt.hash(req.body.Password, UserOptions.saltRounds, function(err,hash){
						//remove pre
						//salt = salt.split("$");
						//salt = salt[salt.length -1];

						//data structure
						//Email, Username, First Name, Last Name, Password
						//database
						//Email, Username, LastName, FirstName, password, Salt, lastLogin, birthDate
						DB().insertNewUser(req.body.Email, req.body.Username, req.body["Last Name"],
							req.body["First Name"], hash, 1,1, function(err, message){
								if (err) return callback(err);							
								callback(false, true, "Success");
							});	
						
					});
				});
				
			}	

			
		});

	}

	User.prototype.login = function(callback){
		

		DB().getUserName(req.body.Username, function(err, rows){
			if (err) return callback(err);
			if (rows.length == 0){
				return callback(false, false, "Signup");
			}
			
			bcrypt.compare(req.body.Password, rows[0].Password, function(err, res){
				if (err) return callback(err);
				if (res){
					req.session.user = rows[0].Username;
					
					callback(false, true,"Logged");
				}
				else{
					callback(false, false,"Password");
				}
				
				//return callback(err, res);
			});
			//callback(err, rows);
		});
	}

	User.prototype.logout = function(callback){
		if (typeof req.session.csrfToken !== "undefined"){
		
			req.session.csrfToken = null;
			
		}
		req.session.destroy();
	}

	User.prototype.getUserName = function(callback){
		if (callback){
			return callback(req.session.user);
		}

		return req.session.user;

	}	
}


function Token(){};

Token.prototype.genCSRF = function(callback){
	var error = false;
	try {
		var salt = this.base64Random(UserOptions.saltRounds)
		var hash = crypto
    		.createHash('sha512')
    		.update(salt)
    		.digest('base64');
	}
	catch (e){
		error = e;
	}
	
    callback(error, hash);
}

Token.prototype.base64Random = function(numBytes) {
	var bytes = crypto.randomBytes(numBytes);
	return new Buffer(bytes, 'binary').toString('base64');
}

function Session(req,res){
	var Tokens = new Token();
	Session.prototype.isAuth = function(callback){
		
		var err = null;
		var state = false;
		var message = null;

		if (!req.session.user){
			message = "no user";
		}
		else {
			//must have auth session, express takes care of expiry
			state = true;
		}
		
		if (typeof callback == "function"){
			return callback(err, state, message);	
		}
		else {
			return state;	
		}
		
	}

	Session.prototype.genCSRF = function(){
		if (typeof req.session.csrfToken === "undefined"){
			console.log(Tokens);
			Tokens.genCSRF(function(err, hash){
				if (err) throw new Error(err);
				
				req.session.csrfToken = hash;
				
			});
			//this.regenCSRF();
			//req.session.csrfToken = this.regenCSRF();
		}
	}

	Session.prototype.regenCSRF = function(callback){
		Tokens.genCSRF(function(err, hash){
			if (err) throw new Error(err);

			req.session.csrfToken = hash;
			
		});
	}

	Session.prototype.getCSRF = function(){
		return req.session.csrfToken;
	}
}







module.exports = User;