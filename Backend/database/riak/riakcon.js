var Riak = require('basho-riak-client');
var async = require('async');
var config = require('../../config');


var client = new Riak.Client([
    config.server + ':10017',
    config.server + ':10027',
    config.server + ':10037',
    config.server + ':10047'
]);

module.exports = client;


/*
function client_shutdown() {
    client.shutdown(function (state) {
        if (state === Riak.Cluster.State.SHUTDOWN) {
            process.exit();
        }
    });
}


var assert = require('assert');

client.ping(function (err, rslt) {
    if (err) {
        throw new Error(err);
    } else {
        // On success, ping returns true
        console.log('working');
        console.log(rslt);
        assert(rslt === true);
    }
});


var testBucket = [
	 {
        emailAddress: "bashoman@basho.com",
        firstName: "Basho",
        lastName: "Man"
    },
    {
        emailAddress: "johndoe@gmail.com",
        firstName: "John",
        lastName: "Doe"
    }
];



var storeFuncs = [];
testBucket.forEach(function (person) {
    // Create functions to execute in parallel to store people
    storeFuncs.push(function (async_cb) {
        client.storeValue({
                bucket: 'testBucket',
                key: person.emailAddress,
                value: person
            },
            function(err, rslt) {
                async_cb(err, rslt);
            }
        );
    });
});

//callback fires when all tasks complee
async.parallel(storeFuncs, function (err, rslts) {
    if (err) {
        throw new Error(err);
    }
    else {
    	
    	client.listBuckets({},function(err,res){
			if (err){
				throw new Error(err);
			}
			else {
				console.log(res);
			}
		});
		
		client.fetchValue({bucket: 'testBucket', key: 'bashoman@basho.com', convertToJs: true},
			function (err, res){
				if (err){
					throw new Error(err);					
				}
				else {
					console.log(res);
				}
			});



    }
});






*/