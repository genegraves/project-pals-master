var client = require('./riakcon');
var async = require('async');
var Riak = require('basho-riak-client');

/*
API

DB().get({bucket:"projects"},"key",callback);
DB().push({bucket:"projects"},[],callback);
*/

var checkArgs = function(options,data,callback,cb){
    if (arguments.length < 4){
        //cb("RiakDb requires three arguments");
        throw new Error("RiakDB requires three arguments");
    }
    else if (typeof cb === "undefined"){
        throw new Error("checkArgs requires a callback as fourth arg");
    }
    else if (typeof cb !== "function"){
        throw new Error("fourth arg must be a function");
    }
    else if (typeof options === "undefined"){
        cb("options must be passed as argument");
    }
    else if (typeof options !== "object"){
        cb("1st argument must be a object");
    }
    else if (typeof data === "undefined"){
        cb("data must be passed as argument");
    }
    /*
    else if (!data instanceof Array && typeof data !== "string"){
        cb("2nd argument must be an Array, String or Object");
    }
    */
    else if (typeof callback === "undefined"){
        cb("callback must be passed as argument");
    }
    else if (typeof callback !== "function"){
        //cb("3rd argument must be a function");
        throw new Error("3rd argument must a function");
    }
    /*
    else if (typeof options['bucket'] === 'undefined'){
        cb("options argument must provide a bucket as KV");
    }
   */
    else {
        cb(null);
    }
}
function RiakDB (){}
//CALLBACK VERSION

RiakDB.prototype.get = function(options,data,callback){
    checkArgs(options,data,callback,
        function(err){
            if (err){
                return callback(err);
            }

            //for single search
            if (typeof data === "string" || typeof data === "number"){
                var temp = [];
                data = {key: data};
                temp.push(data);
                data = temp;
            }

            

            var storeFuncs = [];
            data.forEach(function (obj){

                 if (typeof obj === "string" || typeof obj === "number"){
                    var temp = [];
                    obj = {key: obj};                    
                }

                if (typeof obj['key'] === "undefined" && typeof obj[options.key] === "undefined"){
                    return callback("each object argument must provide a key as KV or specify a key name in options object");
                }

                var key = options.key || "key";

                storeFuncs.push(function(async_cb){
                    client.fetchValue({
                        bucket: options.bucket,
                        key: obj[key],
                        convertToJs: true
                        
                    },
                    function(err, res){
                        async_cb(err, res);
                    });
                });
            });
            //callback: err, rslts
            async.parallel(storeFuncs, callback);
        
        });
    
}

RiakDB.prototype.push = function(options,data,callback){
    checkArgs(options,data,callback,
        function(err){
            if (err){
                return callback(err);
            }
            
            
            var storeFuncs = [];
            data.forEach(function (obj){
                if (typeof obj['key'] === "undefined" && typeof obj[options.key] === "undefined"){
                    return callback("each object argument must provide a key as KV or specify a key name in options object");
                }

                var key = options.key || "key";

                storeFuncs.push(function(async_cb){
                    client.storeValue({
                        bucket: options.bucket,
                        key: obj[key],
                        value: obj
                    },
                    function(err, res){
                        async_cb(err, res);
                    });
                });
            });

            async.parallel(storeFuncs, callback);
        
        });
    
}

RiakDB.prototype.remove = function(options,data,callback){
    checkArgs(options,data,callback,function(err){
        if (err) throw new Error(err);

        if (typeof data['key'] === "undefined" && typeof data[options.key] === "undefined"){
            return callback("each object argument must provide a key as KV or specify a key name in options object");
        }

        var key = options.key || "key";

        client.deleteValue({ bucket: options.bucket, key: data[key]}, function (err, rslt) {
            callback(err,rslt);
        });
    });
    
}

RiakDB.prototype.update = function(options,data,callback){
    //assume no options
    if (arguments.length == 2){
        callback = data;
        data = options;
        options = {};
    }
    checkArgs(options,data,callback,function(err){
        if (err) throw new Error(err);

        client.storeValue({value:data},callback);
    });
}

RiakDB.prototype.debugBucket = function(options,callback){
    
    var lister = new Riak.Commands.KV.ListKeys.Builder()        
        .withBucket(options.bucket)
        .withStreaming(options.stream)
        .withCallback(callback)
        .build();
    
        client.execute(lister);


}

RiakDB.prototype.debugBuckets = function(options,callback){
    var lister = new Riak.Commands.KV.ListBuckets.Builder()
        .withCallback(callback)
        .build();

    client.execute(lister);
}

RiakDB.prototype.debugRemoveAll = function(options, callback){
    var self = this;
    options["stream"] = false;
    this.debugBucket(options,function(err,res){

        for (var i = 0; i < res.keys.length; i++) {
            
                
            self.remove({bucket:options.bucket},{key:res.keys[i]}, function(err){
                if (err) throw new Error(err);

            });

        }
        callback(err,1);
    });

}

RiakDB.prototype.debugShowAll = function(options, callback){
    var self = this;
    options["stream"] = false;
    this.debugBucket(options,function(err,res){
        delete options.stream;
        self.get(options,res.keys, function(err,res){
            callback(err,res);
        });
        
        
    });
}



/*
RiakDB.prototype.update = function(options,needle,value,callback){
  */ 



module.exports = RiakDB;