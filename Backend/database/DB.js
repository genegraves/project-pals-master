

var MysqlDB = require("./mysql/mysqldb");
var RiakDB = require("./riak/riakdb");

function DB(){
    
}

//prevent currently defined functions from losing scope
var curr = MysqlDB.prototype;

//delegate failed lookups to RiakDB
MysqlDB.prototype = Object.create(RiakDB.prototype);
MysqlDB.prototype.constructor = MysqlDB;

extend(curr, MysqlDB.prototype);
curr = null;


//delegate failed lookups to Mysqldb
DB.prototype = Object.create(MysqlDB.prototype);
DB.prototype.constructor = DB;


function extend(src, target){
	for (var i in src){
		target[i] = src[i];
	}
}

function createDB(options){
	return new DB(options);
}


//expose createDB
module.exports = createDB;

