var pool = require('./mysqlcon');
//var Q = require('q');

/*
API USE
var MysqlDB = require('./database/MysqlDB');
//for callback version
db.MysqlDB().getAllUsers(function(err, res){
   if (err) //handle
   console.log(res);
});
//for promise version
MysqlDB.getAllUsers().then(function(users){
  console.log(users);
});
*/




function MysqlDB (){}
//CALLBACK VERSION
MysqlDB.prototype.asyncQuery = function(sql) {
    console.log(sql);
        return function(){
            //var result = Q.defer(),
            var paramsArray = [].slice.call(arguments);
            var callback = paramsArray[paramsArray.length - 1];

            if (typeof callback != 'function'){
                throw 'must pass callback with query';
            }
            
            pool.getConnection(function(err, con) {
                if (err) return callback(err);
               // con.query(sql, paramsArray, result.makeNodeResolver());
                con.query(sql, paramsArray, function(err, rows){
                    if (err) return callback(err);
                    if (rows){
                        callback(false, rows);
                      
                    }
                    con.release();
                    
              });
            });
            
        }
}




MysqlDB.prototype.getAllUsers = MysqlDB.prototype.asyncQuery('SELECT * FROM Users');
MysqlDB.prototype.getFirstName = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE FirstName = ?');
MysqlDB.prototype.getUserPassword = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE UserName = ? AND password = ?');
MysqlDB.prototype.getUserName = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE UserName = ?');
MysqlDB.prototype.getUserEmail = MysqlDB.prototype.asyncQuery('SELECT * FROM Users WHERE Email = ?');


MysqlDB.prototype.insertNewUser = MysqlDB.prototype.asyncQuery("INSERT INTO `Users` (" +
                                                    "`id` ,`Email`, `Username`, `LastName`, `FirstName`, `Password`, `lastLogin`, `birthDate`)"+
                                                    " VALUES ("+                                                        
                                                    "NULL, ?, ?, ?, ?, ?, ?, ?);");
                                                    //Email, Username, LastName, FirstName, Password, Salt, lastLogin, birthDate
//expose factory
module.exports = MysqlDB;