var config = require('../../config');
var mysql = require('mysql');

var pool = mysql.createPool({
  database: config.mysqlConfig.database,
  host : config.mysqlConfig.host,
  user : config.mysqlConfig.user,
  password : config.mysqlConfig.password
  
});

//export singleton pool, ensures one pool only is made
module.exports = exports = pool;