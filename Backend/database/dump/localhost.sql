-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 22, 2015 at 06:06 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ppals`
--

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(22) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL COMMENT 'For users'' email',
  `Username` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `Password` varchar(60) NOT NULL,
  `lastLogin` int(11) NOT NULL,
  `birthDate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `Email`, `Username`, `LastName`, `FirstName`, `Password`, `lastLogin`, `birthDate`) VALUES
(14, 'a@a.com', 'asdf', 'asdf', 'asdf', '$2a$10$tEptg3tJa.AJ.Lfrif4wruM9C.AotdodrEjkaJZKHDCJsasITD/XK', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `expires` int(22) unsigned NOT NULL,
  `data` text COLLATE utf8_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `expires`, `data`) VALUES
('1VmgD_zGoU4C3pNJkYu-SrK6hnRDyq9N', 1432342396, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"secret":"AdJ7_54VFvhUOdmEGXw2OOfl","salt":"tdDOnrf5G9BNihuTuf","csrfToken":"tdDOnrf5G9BNihuTuf-V99rSDtN5Z-eV70_UEEgirNL6Ts"}'),
('95zf3c-hF9Qxks4YMOASHbXSRn121QtZ', 1432357805, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"secret":"YVgkMa_lnMP1eAR4YCZ1-zbN","salt":"Cuyo3ZCGj0PcCxaZDB","csrfToken":"47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=","user":"asdf"}'),
('Bgjp-sDsjIrVkGMSiC9PwgeWwuAAe8PD', 1432342449, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"secret":"efRsj6EW_ZE13SaXIF4HeEKU","salt":"oNERF8iVnTK1d52wPZ","csrfToken":{"_binding":{}}}'),
('bWU8sTAe3DrwFx2nC2dK97LIcpmRDyjU', 1432357829, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"csrfToken":"47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=","user":"asdf"}'),
('m6gdP_covAQgmBFbcVjEXmMIXetJ7C2N', 1432331104, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"csrfToken":"randomString","csrfSecret":"5ecnYgE_aQMQrC_z_QI2-nFd","secret":"0p_0gaC2leJw2lnpDY47U-WM","salt":"mWoUybvPFbUQUENNiy"}'),
('mjXvgHNMbJqeK3n2xiwFJ1RAwWZYWReC', 1432342475, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"secret":"c4EUVwAD6cI2bo7WKHeFf5ZC","salt":"uqb5gLt0NswOCyQZuV","csrfToken":"47DEQpj8HBSa-_TImW-5JCeuQeRkm5NMpJWZG3hSuFU"}'),
('vtidkuwsoB3ZMclyVYyF0kcFYvxMC9rE', 1432333129, '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"secret":"3wxFuTpvxTBSlGBpbQT8ZJM1","salt":"ZgjYMvgoD2ICipu3mv","csrfToken":"ZgjYMvgoD2ICipu3mv-JCzQjLcJ8YvXC4Danwht104Idr8"}');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
