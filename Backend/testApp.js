//cannot read secure cookie unless accessed via HTTPS, so secure is false

var express = require('express'),
    path = require('path'),
    http = require('http'),
    fs = require('fs'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    SessionStore = require('express-mysql-session'),
    mysql = require('mysql');


var yourPort = 80;

var root = path.normalize(__dirname + '/../Frontend');
var app = express();

var server = http.createServer(app).listen(yourPort);
console.log("HTTP server started " + yourPort);

//logger.log('HTTPS server started ' + httpsPort);

// Set up the Socket.IO server, HTTP
//com.io = require('socket.io').listen(server);

app.use(express.static(root));

