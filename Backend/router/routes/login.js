// URL: /login

var com = require('../../app');

var router = require('express').Router();
var User = require("../../User/User");

/*
router.get('/*', function(req, res, next) {
   res.redirect('/') ;
});
*/

router.get('/', function(req,res,next){
  var user = new User(req,res);

    user.Session.genCSRF();
    
    if (!user.Session.isAuth()){
      //res.redirect("/login");
    }
    else {
      return res.redirect("/dashboard");
    }
    
  /*
  //FOR HTTPS
  if (!req.secure){
    var requestHost = req.headers.host.split(':');
    requestHost = requestHost[0];
    var redirectURL = 'https://' + requestHost + ':' + httpsPort + req.url;
    console.log(redirectURL);
    return res.redirect(redirectURL);
  }
  */
    //res.cookie('XSRF-TOKEN', req.csrfToken(), {secure: false});

    res.sendFile(com.root + '/loginPolymer.html');  


});


router.post('/', function(req, res, next) {
  var user = new User(req,res);
  var params = req.body;
  var params = req.body;
  if (typeof params._csrf !== "undefined"
    && params._csrf === user.Session.getCSRF()){
    user.login(function(err,state,message){
      if (err) throw new Error(err);

      res.json({message: message, state: state});
    });
  }
   
   /*
  
	//err for exceptions, user for authen. validation and info for messages
  com.passport.authenticate('local', function(err, user, info) {
    
  	console.log('err: ' + err);
  	console.log('user: ' + user);
  	console.log('info: ' + info);
  	
    if (err) { 
      com.l.log('login error: ' + err);
      return next(err); 
    }
    // Redirect to login if login fails
    if (!user) { 
      
      return res.redirect('/login'); 
      
    }
    req.logIn(user, function(err){
    	if (err) { 
        return next(err); 
      }
    	//req.session.messages = ['foo'];
      com.l.log("User: " + user.username + " logged in.");
      
      return res.redirect('/dashboard');

    });
    
  })(req, res, next);
*/
});



module.exports = router;