 //exports a namespace

var com = require('../../app');
var router = require('express').Router();
var User = require("../../User/User");
var DB = require("../../database/DB");
/*
router.get('/', function(req,res,next){
 	console.log(req.params);
	res.redirect('/dashboard');
	
});
*/

router.get('/', function(req, res, next) {
	res.sendFile(com.root + "/debugConsole.html");
	
});

router.get('/deleteAll', function(req, res, next){
	if (typeof req.param('bucket') === "undefined"){
		return res.send("need bucket arg");
	}
	var bucket = req.param("bucket");

	DB().debugRemoveAll({bucket:bucket}, function(err,rs){
	  if (err) throw new Error(err);

	});
	res.send("success");
});

router.get("/listKeys", function(req,res,next){
	if (typeof req.param('bucket') === "undefined"){
		return res.send("need bucket arg");
	}
	var bucket = req.param("bucket");
	DB().debugBucket({bucket:bucket, stream:false}, function(err,reslt){
	 	if (err) throw new Error(err);
	 	res.send(reslt);
	});
});

router.get("/listAll", function(req,res,next){
	if (typeof req.param('bucket') === "undefined"){
		return res.send("need bucket arg");
	}
	var bucket = req.param("bucket");
	DB().debugShowAll({bucket:bucket, stream:false}, function(err,reslt){
	 	if (err) throw new Error(err);
	 	if (typeof req.param('show') !== "undefined"){
	 		//reslt = reslt.shift().
	 	}
	 	res.send(reslt);
	});
});


module.exports = router;