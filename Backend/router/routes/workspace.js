 //exports a namespace

var com = require('../../app');
var router = require('express').Router();
var User = require("../../User/User");
var DB = require("../../database/DB");
/*
router.get('/', function(req,res,next){
 	console.log(req.params);
	res.redirect('/dashboard');
	
});
*/

router.get('/', function(req, res, next) {
	
	if (typeof req.param('project') === 'undefined'){
		return res.redirect('/dashboard');
	}

	var user = new User(req,res);

	if (!user.Session.isAuth()){

		res.redirect("/login");
	}
	else {
		res.sendFile(com.root + "/chatWorkspace.html");
	}


	
});

router.get('/chats', function(req,res,next){
	console.log(req.param("chatID"));
	if (typeof req.param('chatID') === 'undefined'){
		return res.end();
	}
	var chatID = req.param('chatID');
	DB().get({bucket:"projects"},chatID, function(err,reslt){
	  if (err) throw new Error(err);
	  reslt = reslt.shift().values.shift().value;
	  var chats = reslt.chats;
	  res.send(chats);
	  

	
	  
	});
});


module.exports = router;