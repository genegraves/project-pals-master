// URL: /login

var com = require('../../app');

var router = require('express').Router();
var User = require("../../User/User");

/*
router.get('/*', function(req, res, next) {
   res.redirect('/') ;
});
*/

//send csrf token
router.get('/', function(req,res,next){
  //var user = new User(req,res);
  /*
  //FOR HTTPS
  if (!req.secure){
    var requestHost = req.headers.host.split(':');
    requestHost = requestHost[0];
    var redirectURL = 'https://' + requestHost + ':' + httpsPort + req.url;
    console.log(redirectURL);
    return res.redirect(redirectURL);
  }
  */
  if (typeof req.session.csrfToken !== "undefined"){
    res.json({"csrfToken": req.session.csrfToken});
  }
  else{
    res.end();
  }
  


});


router.post('/', function(req, res, next) {
   console.log(req.body);

  res.send("");
   /*
  
	//err for exceptions, user for authen. validation and info for messages
  com.passport.authenticate('local', function(err, user, info) {
    
  	console.log('err: ' + err);
  	console.log('user: ' + user);
  	console.log('info: ' + info);
  	
    if (err) { 
      com.l.log('login error: ' + err);
      return next(err); 
    }
    // Redirect to login if login fails
    if (!user) { 
      
      return res.redirect('/login'); 
      
    }
    req.logIn(user, function(err){
    	if (err) { 
        return next(err); 
      }
    	//req.session.messages = ['foo'];
      com.l.log("User: " + user.username + " logged in.");
      
      return res.redirect('/dashboard');

    });
    
  })(req, res, next);
*/
});

router.get('/profile', function(req,res,next){
    var user = new User(req,res);

    
    if (!user.Session.isAuth()){
      return res.end();
    }
    else{
      
      res.json({username: user.getUserName()});
    }
});




module.exports = router;