// URL: /login

var com = require('../../app');

var router = require('express').Router();
var User = require("../../User/User");

/*
router.get('/*', function(req, res, next) {
   res.redirect('/') ;
});
*/

router.get('/', function(req,res,next){
  var user = new User(req,res);

    user.Session.genCSRF();
    console.log(req.session);
    if (!user.Session.isAuth()){
      //res.redirect("/login");
    }
    else {
      res.redirect("/dashboard");
    }
    console.log(req.session);
  /*
  //FOR HTTPS
  if (!req.secure){
    var requestHost = req.headers.host.split(':');
    requestHost = requestHost[0];
    var redirectURL = 'https://' + requestHost + ':' + httpsPort + req.url;
    console.log(redirectURL);
    return res.redirect(redirectURL);
  }
  */
    //res.cookie('XSRF-TOKEN', req.csrfToken(), {secure: false});

    res.sendFile(com.root + '/signupPolymer.html');  


});


router.post('/', function(req, res, next) {
  var user = new User(req,res);
  var params = req.body;
  if (typeof params._csrf !== "undefined"
    && params._csrf === user.Session.getCSRF()){
      user.register(function(err,state, message){
        
        if (err) throw new Error(err);
        res.json({message:message,state:state});

      });
   
  }
  
  

});



module.exports = router;