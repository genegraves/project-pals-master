//exports a namespace

	var com = require('../../app');
	var router = require('express').Router();
	var User = require("../../User/User");

    router.get('/', function(req,res,next){
 	var user = new User(req,res);
 
		if (!user.Session.isAuth()){

			res.redirect("/login");
		}
		else {
			res.sendFile(com.root + '/logged.html');
		}
	});
	
	module.exports = router;

