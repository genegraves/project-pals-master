module.exports = function (){
	var com = require('../app');
	var router = require('express').Router();
	var User = require("../User/User");


	com.app.get('/', function(req,res,next){
		
		var user = new User(req,res);

		
		if (!user.Session.isAuth()){
			res.redirect("/login");
		}
		else{
			res.redirect("/dashboard");
		}
		
	});

	com.app.use('/workspace', require('./routes/workspace'));
	
	com.app.use('/signup', require('./routes/signup'));
	com.app.use('/login', require('./routes/login'));
	com.app.use('/dashboard', require('./routes/dashboard'));
	com.app.use('/user', require('./routes/user'));
	com.app.use('/logout', require('./routes/logout'));
	com.app.use('/debug', require('./routes/debug'));

	//otherwise a 404
	com.app.use(function(req, res, next){
	  res.status(404);
	  /*
	  // respond with html page
	  if (req.accepts('html')) {
	    res.render('404', { url: req.url });
	    return;
	  }

	  // respond with json
	  if (req.accepts('json')) {
	    res.send({ error: 'Not found' });
	    return;
	  }
	*/
	  // default to plain-text. send()
	  res.type('txt').send('Not found');
	});

	//com.app.use(com.subdomain('login', require('./routes/login')));
}();