var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended:true
}));


 var port = process.env.PORT || 3000;
var root = path.normalize(__dirname + '/../Frontend');
console.log(root);
 app.use(express.static(root));

app.get('/', function(req, res, next){
	console.log(__dirname + '/../Frontend');
	res.redirect('./RESTful-elem/index.html');
	next();
});

app.post('/rest', function(req,res,next){
	var params = req.body;
	console.log(params);
	
	res.json({
		"detail":{
			"message":req.body.profInfo,
			"other":req.body.otherInfo,
			"stuff":"more stuff"
		},
		"type":"profile"

	});
	
});

app.get('/profile', function(req,res,next){
	
	res.json({
		"detail":{
			"name":"Aidan Anderson",
			"email":"aanderson@projectpals.com"
		},
		"type":"profile"

	});
	next();
});

app.get('/profile/about', function(req,res,next){
	
	res.json({
		"detail":{
			"activity":"I like hiking.",
			"desc":"blah blah blah"

		},
		"type":"about"

	});
	next();
});

app.get('/messages', function(req,res,next){
	
	res.json({
		"detail":{
			"1":"message one",
			"2":"message two",
			"3":"message three"
		},
		"type":"messages"

	});
	next();
});




console.log("listening on " + port);
app.listen(port);