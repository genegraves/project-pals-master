//cannot read secure cookie unless accessed via HTTPS, so secure is false

var express = require('express'),
    path = require('path'),
    http = require('http'),
    fs = require('fs'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    SessionStore = require('express-mysql-session'),
    mysql = require('mysql');

var com = {
  config: require("./config"),
  
};

com.pool = mysql.createPool({
  database: com.config.mysqlConfig.database,
  host : com.config.mysqlConfig.host,
  user : com.config.mysqlConfig.user,
  password : com.config.mysqlConfig.password
  
});

//disabled for time being
//com.root = path.normalize(__dirname + '/../Frontend');
com.app = express();


var server = http.createServer(com.app).listen(com.config.port);
console.log("HTTP server started " + com.config.port);
com.io = require('socket.io').listen(server);
console.log("WebSockets listening " + com.config.port);



// Set up the Socket.IO server, HTTP
//com.io = require('socket.io').listen(server);

//com.app.use(express.static(com.root));



var sessionStore = new SessionStore(com.config.mysqlConfig);
 
 
com.app.use(bodyParser.json());
com.app.use(bodyParser.urlencoded({
	extended: true
}));
com.app.use(cookieParser());

var sessionMiddleware = session({
	store: sessionStore,
	secret: com.config.secret
});

com.app.use(sessionMiddleware);

//pass session tos socket.io
com.io.use(function(socket,next){
  sessionMiddleware(socket.request, socket.request.res, next);
});



module.exports = com;



//require('./passportConfig');

var router = require('./router/router');

var WSRouter = require('./socketRoutes/socketCon');










